package ru.t1.zkovalenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @NotNull final String name, @Nullable final String description) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description == null ? "" : description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

}
