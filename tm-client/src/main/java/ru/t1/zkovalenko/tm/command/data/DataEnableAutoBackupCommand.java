package ru.t1.zkovalenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.data.DataEnableAutoBackupRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;

public class DataEnableAutoBackupCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-enable-auto-backup";

    @NotNull
    public static final String ARGUMENT = "-deab";

    @NotNull
    public static final String DESCRIPTION = "Enable auto backup at the server";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[ENABLE AUTO BACKUP]");
        @NotNull DataEnableAutoBackupRequest request = new DataEnableAutoBackupRequest(getToken());
        getDomainEndpoint().enableAutoBackupData(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
