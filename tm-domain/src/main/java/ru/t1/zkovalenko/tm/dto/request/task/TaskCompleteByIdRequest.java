package ru.t1.zkovalenko.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskCompleteByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    public TaskCompleteByIdRequest(@Nullable String token) {
        super(token);
    }

}
