package ru.t1.zkovalenko.tm.repository;

import ru.t1.zkovalenko.tm.api.repository.ISessionRepository;
import ru.t1.zkovalenko.tm.model.Session;

public class SessionRepository extends AbstractUserOwnerRepository<Session> implements ISessionRepository {

}
