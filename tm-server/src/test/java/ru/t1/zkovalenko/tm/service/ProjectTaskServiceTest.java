package ru.t1.zkovalenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.api.service.IProjectService;
import ru.t1.zkovalenko.tm.api.service.IProjectTaskService;
import ru.t1.zkovalenko.tm.api.service.ITaskService;
import ru.t1.zkovalenko.tm.marker.UnitCategory;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.model.Task;
import ru.t1.zkovalenko.tm.repository.ProjectRepository;
import ru.t1.zkovalenko.tm.repository.TaskRepository;

import static ru.t1.zkovalenko.tm.constant.ProjectTestData.PROJECT_NAME;
import static ru.t1.zkovalenko.tm.constant.TaskTestData.TASK_NAME;
import static ru.t1.zkovalenko.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Nullable
    private Project projectCreated;

    @Nullable
    private Task taskCreated;

    @Nullable
    private Task taskFounded;

    @Before
    public void addProjectTask() {
        projectCreated = projectService.create(USER1.getId(), PROJECT_NAME);
        taskCreated = taskService.create(USER1.getId(), TASK_NAME);
        projectTaskService.bindTaskToProject(USER1.getId(), projectCreated.getId(), taskCreated.getId());
        taskFounded = taskRepository.findAllByProjectId(USER1.getId(), projectCreated.getId()).get(0);
    }

    @After
    public void after() {
        projectService.clear();
        taskService.clear();
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertEquals(taskCreated, taskFounded);
    }

    @Test
    public void removeProjectById() {
        Assert.assertNotNull(taskFounded);
        projectTaskService.removeProjectById(USER1.getId(), projectCreated.getId());
        Assert.assertNull(taskService.findOneById(taskFounded.getId()));
    }

    @Test
    public void removeProjectByIndex() {
        Assert.assertNotNull(taskFounded);
        projectTaskService.removeProjectByIndex(USER1.getId(), 0);
        Assert.assertNull(taskService.findOneById(taskFounded.getId()));
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertNotNull(taskFounded);
        projectTaskService.unbindTaskFromProject(USER1.getId(), projectCreated.getId(), taskCreated.getId());
        final int taskSize = taskRepository.findAllByProjectId(USER1.getId(), projectCreated.getId()).size();
        Assert.assertEquals(0, taskSize);
    }

}
