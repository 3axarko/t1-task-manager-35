package ru.t1.zkovalenko.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove project by id";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken());
        request.setProjectId(id);
        getProjectEndpoint().removeByIdProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
