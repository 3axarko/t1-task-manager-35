package ru.t1.zkovalenko.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.AbstractRequest;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserRequest extends AbstractRequest {

    @Nullable
    private String token;

    public AbstractUserRequest(@Nullable String token) {
        this.token = token;
    }

}
