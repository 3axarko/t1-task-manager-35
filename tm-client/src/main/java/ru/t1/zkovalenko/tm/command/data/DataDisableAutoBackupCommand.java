package ru.t1.zkovalenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.data.DataDisableAutoBackupRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;

public class DataDisableAutoBackupCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-disable-auto-backup";

    @NotNull
    public static final String ARGUMENT = "-ddab";

    @NotNull
    public static final String DESCRIPTION = "Disable auto backup at the server";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DISABLE AUTO BACKUP]");
        @NotNull DataDisableAutoBackupRequest request = new DataDisableAutoBackupRequest(getToken());
        getDomainEndpoint().disableAutoBackupData(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
