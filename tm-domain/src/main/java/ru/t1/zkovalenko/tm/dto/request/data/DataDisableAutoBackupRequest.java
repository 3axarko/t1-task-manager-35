package ru.t1.zkovalenko.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;

@NoArgsConstructor
public class DataDisableAutoBackupRequest extends AbstractUserRequest {

    public DataDisableAutoBackupRequest(@Nullable String token) {
        super(token);
    }

}
