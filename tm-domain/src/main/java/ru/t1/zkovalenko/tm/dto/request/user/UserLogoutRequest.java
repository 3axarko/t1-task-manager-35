package ru.t1.zkovalenko.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class UserLogoutRequest extends AbstractUserRequest {

    private String login;

    public UserLogoutRequest(@Nullable String token) {
        super(token);
    }

}
