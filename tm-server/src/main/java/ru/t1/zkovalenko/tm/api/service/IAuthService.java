package ru.t1.zkovalenko.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.Session;
import ru.t1.zkovalenko.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    @SneakyThrows
    Session validateToken(@Nullable String token);

    String login(@Nullable String login, @Nullable String password);

    void logout(@Nullable Session session);

}
