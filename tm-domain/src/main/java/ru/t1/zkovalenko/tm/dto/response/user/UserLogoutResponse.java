package ru.t1.zkovalenko.tm.dto.response.user;

import lombok.NoArgsConstructor;
import ru.t1.zkovalenko.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public class UserLogoutResponse extends AbstractResponse {
}
