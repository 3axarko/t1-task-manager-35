package ru.t1.zkovalenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.repository.ISessionRepository;
import ru.t1.zkovalenko.tm.marker.UnitCategory;
import ru.t1.zkovalenko.tm.model.Session;

import static ru.t1.zkovalenko.tm.constant.SessionTestData.SESSION1;
import static ru.t1.zkovalenko.tm.constant.SessionTestData.SESSION2;
import static ru.t1.zkovalenko.tm.constant.UserTestData.USER1;
import static ru.t1.zkovalenko.tm.constant.UserTestData.USER2;
import static ru.t1.zkovalenko.tm.enumerated.Sort.BY_NAME;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository repository = new SessionRepository();

    @Test
    public void add() {
        @Nullable final Session session = repository.add(SESSION1);
        Assert.assertEquals(session, repository.findOneById(session.getId()));
    }

    @Test
    public void remove() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @Nullable final Session session1 = repository.add(USER1.getId(), SESSION1);
        @Nullable final Session session2 = repository.add(USER2.getId(), SESSION2);
        repository.remove(USER1.getId(), session1);
        Assert.assertNotEquals(session2, repository.findOneById(USER1.getId(), session2.getId()));
        Assert.assertEquals(session2, repository.findOneById(USER2.getId(), session2.getId()));
    }

    @Test
    public void clearAndGetSize() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1.getId(), SESSION1);
        repository.add(USER1.getId(), SESSION2);
        Assert.assertEquals(2, repository.getSize(USER1.getId()));
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void findAllWithOneParam() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @Nullable final Session session = repository.add(USER1.getId(), SESSION1);
        Assert.assertEquals(session, repository.findAll(USER1.getId()).get(0));
    }

    @Test
    public void findAllWithTwoParams() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @Nullable final Session session = repository.add(USER1.getId(), SESSION1);
        Assert.assertEquals(session, repository.findAll(USER1.getId(), BY_NAME.getComparator()).get(0));
    }

    @Test
    public void existById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @Nullable final Session session = repository.add(USER1.getId(), SESSION1);
        Assert.assertTrue(repository.existById(session.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @Nullable final Session session = repository.add(USER1.getId(), SESSION1);
        Assert.assertEquals(session, repository.findOneById(USER1.getId(), session.getId()));
    }

    @Test
    public void findOneByIndex() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @Nullable final Session session = repository.add(USER1.getId(), SESSION1);
        Assert.assertEquals(session, repository.findOneByIndex(USER1.getId(), 0));
    }

    @Test
    public void removeById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @Nullable final Session session1 = repository.add(USER1.getId(), SESSION1);
        @Nullable final Session session2 = repository.add(USER1.getId(), SESSION2);
        repository.removeById(session2.getId());
        Assert.assertEquals(session1, repository.findAll(USER1.getId()).get(0));
    }

    @Test
    public void removeByIndex() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @Nullable final Session session1 = repository.add(USER1.getId(), SESSION1);
        repository.add(USER1.getId(), SESSION2);
        repository.removeByIndex(1);
        Assert.assertEquals(session1, repository.findAll(USER1.getId()).get(0));
    }

}
