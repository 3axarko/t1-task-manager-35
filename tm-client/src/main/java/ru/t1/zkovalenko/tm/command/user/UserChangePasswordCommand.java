package ru.t1.zkovalenko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.exception.field.PasswordEmptyException;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "change-password";

    @NotNull
    private final String DESCRIPTION = "User change password";

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("Enter new password:");
        @NotNull final String password = TerminalUtil.nextLine();
        if (password.isEmpty()) throw new PasswordEmptyException();
        UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setPassword(password);
        getUserEndpoint().changeUserPassword(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
