package ru.t1.zkovalenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.data.DataXmlLoadJFasterXmlRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;

public class DataXmlLoadJFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml-fasterxml";

    @NotNull
    public static final String ARGUMENT = "-dlxf";

    @NotNull
    public static final String DESCRIPTION = "Load data xml FasterXML from file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML FASTERXML LOAD]");
        @NotNull DataXmlLoadJFasterXmlRequest request = new DataXmlLoadJFasterXmlRequest(getToken());
        getDomainEndpoint().xmlLoadJFasterXmlData(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
