package ru.t1.zkovalenko.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.Project;

public interface IProjectRepository extends IUserOwnerRepository<Project> {

    @NotNull
    Project create(@Nullable String userId, @NotNull String name, @Nullable String description);

    @NotNull
    Project create(@Nullable String userId, @NotNull String name);

}
