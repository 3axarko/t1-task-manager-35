package ru.t1.zkovalenko.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class EndpointTestData {

    @NotNull
    public static final String USER_ADMIN_LOGIN = "admin";

    @NotNull
    public static final String USER_ADMIN_PASSWORD = "admin";

}
